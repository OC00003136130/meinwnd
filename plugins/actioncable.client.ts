import { defineNuxtPlugin } from '#app'
import ActionCableVue from "actioncable-vue";
export default defineNuxtPlugin(nuxtApp => {
  const config = useRuntimeConfig()

  nuxtApp.vueApp.use(ActionCableVue, {
    debug: true,
    debugLevel: "all",
    connectionUrl: config.public.WEB_SOCKET_URL,
    connectImmediately: true,
  });
})