export type MapLocation = {
    id: string,
    longitude: number,
    latitude: number,
    draggable: boolean,
    tooltipHtml: string
  }