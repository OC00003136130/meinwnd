import { defineNuxtConfig } from 'nuxt/config'

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  css: ['vuetify/lib/styles/main.sass', '@mdi/font/css/materialdesignicons.min.css', '@/assets/sass/main.sass', 'leaflet/dist/leaflet.css', '@/assets/css/markercluster/MarkerCluster.css', '@/assets/css/markercluster/MarkerCluster.Default.css'],
  build: {
    transpile: ['vuetify'],
  },
  vite: {
    define: {
      'process.env.DEBUG': false,
    },
  },
  modules: [
    '@pinia/nuxt',
    '@nuxtjs/device'
  ],
  runtimeConfig: {
    PUBLIC_API_USERNAME: process.env.PUBLIC_API_USERNAME,
    PUBLIC_API_PASSWORD: process.env.PUBLIC_API_PASSWORD,
    REGISTER_TOKEN: process.env.REGISTER_TOKEN,
    SENDINBLUE_API_KEY: process.env.SENDINBLUE_API_KEY,
    public: {
      WEB_SOCKET_URL: process.env.WEB_SOCKET_URL,
      API_BASE_URL: process.env.API_BASE_URL
    }
  },
  ssr: true,

  app: {
    head: {
      charset: 'utf-16',
      title: 'SMART WENDELER LAND',
      meta: [
        { property: 'og:type', content: 'Webseite' },
        { property: 'og:title', content: 'SMART WENDELER LAND' },
        { property: 'og:description', content: 'project_platform' }
      ],
    }
  }
})