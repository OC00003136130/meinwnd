const errorData = {
    'Invalid credentials': 'Benutzername oder Passwort falsch',
    'user.email.invalid': 'E-Mail Adresse fehlerhaft oder schon vorhanden',
    'category.name.invalid': 'Bitte Kategorien Name eingeben',
    'category.name.taken': 'Diser Kategorien Name ist bereits vergeben',
    'project.name.invalid': 'Bitte Projekt Name eingeben',
    'comment.comment.invalid': 'Bitte einen Kommentar eingeben',
    'reset_password.failed': 'Zurücksetzen des Passworts fehlgeschlagen. E-Mail existiert nicht.',
    'project.start_and_end_time_must_be_in_correct_order': 'Das Start-Datum muss vor dem End-Datum liegen',
    'message.email.invalid': 'Bitte geben Sie eine E-Mail Adresse an',
    'message.subject.invalid': 'Bitte geben Sie einen Betreff Ihrer Nachricht an',
    'message.message.invalid': 'Bitte geben Sie eine Nachricht ein',
    'gamificationgame.name.invalid': 'Bitte gib einen Namen für diese Gamification Serie ein',
    'gamificationreward.name.invalid': 'Bitte gib einen Namen für diese Belohnung ein'
}
export default errorData