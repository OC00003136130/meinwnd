import axios from "axios";
import { getErrorMessage } from "~~/utils/error.utils";
import { ServerCallResult } from "~~/types/serverCallResult";

export default defineEventHandler(async (event): Promise<ServerCallResult> => {
  const { newsLetterEmail } = await readBody(event);

  const config = useRuntimeConfig();

  try {
    const response = await axios.post(
      "https://api.sendinblue.com/v3/contacts/doubleOptinConfirmation",
      {
        email: newsLetterEmail,
        templateId: 1,
        includeListIds: [2],
        redirectionUrl: "https://unser.smartwendelerland.de/newsletter_success",
      },
      {
        headers: {
          "Content-Type": "application/json",
          "api-key": config.SENDINBLUE_API_KEY,
        },
      }
    );

    return ServerCallResult.success(response.data.status, response.data.data);
  } catch (error) {
    if (
      typeof error === "object" &&
      error &&
      "response" in error &&
      typeof error.response === "object" &&
      error.response &&
      "status" in error.response &&
      typeof error.response.status === "number" &&
      "data" in error.response
    ) {
      return ServerCallResult.error(getErrorMessage(error), error.response.status, error.response.data);
    }

    return ServerCallResult.error("Es ist ein unbekannter Fehler aufgetreten", 505, error);
  }
});
