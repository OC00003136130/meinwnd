import { Method } from 'axios'
import { useServerInterface } from '@/composables/server/interface'
import { ServerCallResult } from '@/types/serverCallResult'

export function usePrivateApi() {
  const config = useRuntimeConfig()
  const AUTH_TOKEN = 'auth._token.jwt'

  const router = useRouter()

  const serverInterface = useServerInterface()
  serverInterface.setDomain(config.public.API_BASE_URL)

  if (process.client) {
    serverInterface.setHeaders({
      Authorization: `Bearer ${localStorage.getItem(AUTH_TOKEN)}`,
      'content-type': 'application/json',
      'Request-Source': 'web',
      'Request-Platform': 'browser'
    })
  }

  const call = async (method: Method, url: string, data?: any): Promise<ServerCallResult> => {
    const result = await serverInterface.call(method, url, JSON.stringify(data))

    // Unauthorized because token is probably expired -> delete it.
    if (result.httpCode === 401) {
      if (process.client) {
        localStorage.removeItem(AUTH_TOKEN)
      }
    }

    return result
  }

  return {
    call
  }
}
