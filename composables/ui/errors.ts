import errorData from '@/data/error_data'

export function useErrors() {

  const checkAndMapErrors = (fieldName:string, errors:any) => {
    let errorMessages = []
    if (errors["errors"]) {
      errors["errors"].forEach(error => {
        if (error.field_name === fieldName) {
          errorMessages.push(mapError(error.code))
        }
      })
    }
    return errorMessages
  }

  const mapError = (errorCode:any) => {
    if (errorData[errorCode]) {
      return errorData[errorCode]
    } else {
      return errorCode
    }
  }

  const getErrorMessage = (error: any) => {
    let result: string

    if (error.response) {
      if (error.response.data && error.response.data.msg) {
        result = error.response.data.msg
      }
      else {
        result = 'Ein Fehler ist aufgetreten'
      }
    }
    else if (error.code) {
      result = error.code
    }
    else {
      result = error.message
    }

    return result
  }

  return {
    checkAndMapErrors,
    mapError,
    getErrorMessage
  }
}
