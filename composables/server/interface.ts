import axios, { AxiosInstance, AxiosRequestConfig, Method } from 'axios'
import { ServerCallResult } from '@/types/serverCallResult'
import { useErrors } from '../ui/errors';

export function useServerInterface() {
  
  const errors = useErrors();

  const client: AxiosInstance =  axios.create()

  let domain = null
  let headers = null

  const setDomain = (domain_: string) => {
    domain = domain_
  }

  const setHeaders = (headers_: Object) => {
    headers = headers_
  }

  const call = async (method: Method, url: string, data?: any): Promise<ServerCallResult> => {

    const requestConfig: AxiosRequestConfig = {
      baseURL: domain,
      url: url,
      method: method,
      data: data,
      headers: headers
    }

    try {
      const response = await client.request(requestConfig)
      return ServerCallResult.success(response.status, response.data)
    }
    catch (error) {
      return ServerCallResult.error(errors.getErrorMessage(error), error.response?.status, error.response?.data)
    }
  }

  return {
    setDomain,
    setHeaders,
    call
  }
}
